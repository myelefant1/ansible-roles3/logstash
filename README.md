## Logstash

This role installs logstash.

Tested on debian 10.

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|-----------------------------------------|
| logstash_additional_plugins | list     | yes       | []              | List of the non default plugins you want to add |
| logstash_additional_configuration_files | list     | yes       | []  | List of the files you want to add in logstash configruation|
| logstash_memory_size        | string   | yes       | 256m            | fixed size if logstash jvm memory |
| logstash_service_state      | boolean  | yes       | started         | state of the logstash systemd service |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/elastic_repo.git
  scm: git
  version: master
- src: https://gitlab.com/myelefant1/ansible-roles3/logstash.git
  scm: git
  version: master
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: logstash
      logstash_additional_plugins:
        - logstash-input-cloudwatch
      logstash_additional_configuration_files:
        - logstash_configuration/logstash-input-cloudwatch.conf
        - logstash_configuration/logstash-output-elasticsearch.conf
      logstash_memory_size: 442m
```

## Tests

[tests/tests_logstash](tests/tests_logstash)
[tests/tests_logstash_configuration](tests/tests_logstash_configuration)
[tests/tests_logstash_plugins](tests/tests_logstash_plugins)
